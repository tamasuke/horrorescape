﻿
# include <Siv3D.hpp>

#include "_sequence\SceneManager.h"

struct SharedData
{

};


class EffectSample : public IEffect
{
	Point p;

public:
	EffectSample() : 
		p(0,0)
	{}

	bool update(double t) override
	{
		p = EaseIn(Point(0,0), Point(0,50), Easing::Linear, t);
	}

	Point Get() const { return p; }
};

class M : public Effect
{

};




class Game;

class Title : public Sequence::SceneManager<SharedData>::Scene
{
	const Font font;

	Effect e;
	Point p;
public:
	Title() :
		font(30)
	{

	}

	bool Update() override
	{
		e.update();
		if (Input::KeyO.clicked)
		{
			this->m_state = State::DoNothing;
			this->m_pManager->EmplaceScene<Game>();
		}
		if (Input::KeyP.clicked)
		{
		}

		return true;
	}

	void Draw() const override
	{
		font(L"ようこそ、Siv3D の世界へ！")
			.draw(p);

		

		Circle(Mouse::Pos(), 50).draw({ 255, 0, 0, 127 });
	}
};

class Game : public Sequence::SceneManager<SharedData>::Scene
{

public:
	bool Update() override
	{
		if (Input::KeyO.clicked)
		{
			this->m_state = State::DoNothing;
			this->m_pManager->EmplaceScene<Title>();
		}

		return true;
	}

	void Draw() const override
	{
		Circle(Mouse::Pos(), 50).draw({ 0, 255, 0, 127 });
	}
};

void Main()
{
	Sequence::SceneManager<SharedData> manager;
	manager.EmplaceScene<Title>();

	M m;
	

	while (System::Update())
	{
		if (!manager.Update())
			break;

		manager.Draw();
	}
}
