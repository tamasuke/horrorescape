#pragma once

#include "SequenceManager.h"
#include "../DX/DX11/Sprite.h"
#include "../DX/D2D/D2DString.h"
#include <DirectXTK\SpriteBatch.h>
#include <DirectXTK\CommonStates.h>

struct SharedResource
{
	std::unique_ptr<DirectX::SpriteBatch> renderer;
	std::unique_ptr<DirectX::CommonStates> defaultState;
};

using sceneManager = Sequence::SceneManager < SharedResource >;

class GameMain;

class Title : public sceneManager::Scene
{
	
public:
	Title()
	{
		Init();
	}

	void Init()
	{
		m_state = State::Active;
		m_priority = 1;
	}
	bool Update() override
	{
		return true;
	}
	void Draw() const override
	{
	}

};

class GameMain : public sceneManager::Scene
{
	DX11::Sprite sprite;
	math::Vec2 m_forward;
	D2D::Font font;

public:
	GameMain();

	void Init();
	bool Update() override;
	void Draw() const override;
};
