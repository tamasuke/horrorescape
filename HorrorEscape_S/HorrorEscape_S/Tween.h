#pragma once


class Tween
{
public:
	void Update()
	{

	}


public:
	static Tween& I()
	{
		static Tween instance;
		return instance;
	}
private:
	Tween() = default;
	Tween(const Tween&) = delete;
	Tween& operator=(const Tween&) = delete;
};