#include "GameMain.h"
#include "../System/DXInput.h"
#include "../DX/DX11/TextureCache.h"
#include <random>

namespace Random
{
	int Range(int begin, int end) {
		if (begin < end) {
			static std::random_device randomMaker;
			static std::mt19937 mersenneTwister(randomMaker());

			std::uniform_int_distribution<int> distribution(begin, end);
			return distribution(mersenneTwister);
		}
		else
			return 0;
	}
}

struct InputDir
{
	static math::Vec2 GetVec()
	{
		math::Vec2 vec;
		if (GetDXInput()->GetIsPressedKeyboard(KeyCode::Right))
			vec.x += 1;
		if (GetDXInput()->GetIsPressedKeyboard(KeyCode::Left))
			vec.x -= 1;
		if (GetDXInput()->GetIsPressedKeyboard(KeyCode::Up))
			vec.y -= 1;
		if (GetDXInput()->GetIsPressedKeyboard(KeyCode::Down))
			vec.y += 1;

		return vec;
	}
};

struct Map
{
	std::vector<DX11::Sprite> sprite;
public:

	void Init()
	{
		sprite.assign(22 * 40, DX11::Sprite{});

		for (int i = 0; i < 22; i++)
		{
			for (int j = 0; j < 40; j++)
			{
				sprite.at(i * 40 + j).SetPos(j * 32, i * 32);
				sprite.at(i * 40 + j).Scale(2, 2);
			}
		}
	}
	void Draw(DirectX::SpriteBatch* r) const
	{
		for (auto&& tex : sprite)
		{
			tex.Draw(r,
				TextureCache::GetInstance()->At(L"kusa1")
				.Region({ 0, 64, 16 }));
		}
	}
}map;

DX11::Sprite m_bomb;

GameMain::GameMain() :
	font(D2D::TextFormat(30))
{
	Init();
}

void GameMain::Init()
{
	m_state = State::Active;
	m_priority = 0;

	map.Init();

	m_bomb.SetPos(Random::Range(2 , 38) * 32, Random::Range(2, 22) * 32);
}

bool isMukiau = false;

float dotValue = 0;

bool GameMain::Update()
{
	static float s = 1.f;

	auto vec= InputDir::GetVec();

	if (vec != math::Vec2::Zero)
		m_forward = vec;

	static int dir = 1;
	if (GetDXInput()->GetIsJustPressedKeyboard(KeyCode::Return))
		dir *= -1;

	sprite.Translate(vec * 3.f);

	auto bomb2player = sprite.GetPos() - m_bomb.GetPos();
	auto len = bomb2player.Length();
	bomb2player.Normalize();
	dotValue = m_forward.Dot(bomb2player);

	isMukiau = (-1.1f <= dotValue && dotValue < -0.99);

	if (isMukiau)
	{
		if(len <= 200.f)
		m_bomb.Translate(dir * (6330 / (len *len)) * bomb2player);
	}

	return true;
}

void GameMain::Draw() const
{
	using namespace DirectX;

	auto renderer = m_data->renderer.get();

	m_data->renderer->Begin(SpriteSortMode::SpriteSortMode_Deferred,
		m_data->defaultState->NonPremultiplied());

	map.Draw(renderer);

	m_bomb.Draw(renderer, TextureCache::GetInstance()->At(L"bomb").Region({0,0,32}));

	sprite.Draw(m_data->renderer.get(),
		TextureCache::GetInstance()->At(L"chara").Region({ 0,0, 32, 32 }));


	m_data->renderer->End();

	font(dotValue).Draw(10, 100, 300,100, math::ColorF(math::Palette::Black));
	font(isMukiau).Draw(10, 150, 100,100, math::ColorF(math::Palette::Black));
}
